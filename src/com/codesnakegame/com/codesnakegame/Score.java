package com.codesnakegame;

import processing.core.PApplet;
import processing.core.PImage;



public class Score {
    PApplet g;
    PImage menuB;
    int red;
    String[] top5Scores = new String[5];
    String[] top5S = new String[5];
    String[] top5Names = {"______", "______", "______", "______", "______"};
    boolean[] compare = new boolean[5];
    int[] bufferScore = new int[5];
    String[] bufferNames = new String[5];
    int bufferNum;

    int[] place = {1,2,3,4,5};
    Score(PApplet g, PImage menuB){
        this.g = g;
        this.red = g.color(255, 0, 0);
        this.menuB = menuB;

    }




    void snakeScore(String name, int score){

        top5Scores = g.loadStrings("Top5Scores.txt");
        top5Names = g.loadStrings("Top5Names.txt");
        for (int i = 0; i < top5Scores.length; i++) {
            bufferScore[i] = Integer.parseInt(top5Scores[i]);
            bufferNames[i] = top5Names[i];
            compare[i] = score > bufferScore[i];
        }
        for (int i = 0; i < top5Names.length-1; i++) {
            if (compare[i]){
                    top5Scores[i] = "";
                    top5Scores[i] += score;
                    top5Names[i] = name;

                for (int j = i; j < top5Scores.length-1; j++) {
                    top5Scores[j+1] = "";
                    top5Scores[j+1] += bufferScore[j];
                    top5Names[j+1] = bufferNames[j];
                }
                g.saveStrings("Top5Names.txt", top5Names);
                g.saveStrings("Top5Scores.txt", top5Scores);
                break;
            }
        }



    }

    void topList(){
        g.image(menuB, 400, 500, 129, 54);
        g.fill(red);
        g.textSize(40);
        top5Scores = g.loadStrings("Top5Scores.txt");
        top5Names = g.loadStrings("Top5Names.txt");
        g.text("Top 5 Scores", 170, 100);
        for (int i = 0; i < top5S.length; i++) {
            top5S[i] = place[i] + ". " + top5Names[i] + "   " + top5Scores[i];
            g.textSize(20);
            g.text(top5S[i],100, 70*(i+1)+100);
        }


    }
}
