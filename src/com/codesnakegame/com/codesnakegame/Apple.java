package com.codesnakegame;
import processing.core.*;

public class Apple {
    PApplet g;
    private float scl;
    Apple(PApplet g, float scl){
        this.scl = scl;
        this.g = g;
    }
    private float lowRef;
    private float highRef;
    float xApple;
    float yApple;
    void newApple(){
        lowRef = 4f;
        highRef = (600-40)/scl+0.1f;
        xApple = scl*(int)g.random(lowRef, highRef);
        yApple = scl*(int)g.random(lowRef, highRef);


    }
    void drawApple(){

        g.rect(xApple, yApple, scl, scl);
    }
}
