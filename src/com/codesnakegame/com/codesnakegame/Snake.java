package com.codesnakegame;
import processing.core.*;
  public class Snake {
     float xHead, yHead;
     float xTail, yTail;
      float xDir = 0;
      float yDir = 0;
      float xInt = 0;
      float yInt = 0;
     int check;
     float scl;
      float[] xIntA = new float[100000];
      float[] yIntA = new float[100000];
      float[] xCor = new float[100000];
      float[] yCor = new float[100000];
      int snakeLength;

     PApplet g;
    Snake(PApplet g, float scl){
        this.scl = scl;
        this.g = g;
    }
    float lowRef;
    float highRef;
      public  void keyPressed1(){
          if (g.keyCode == g.UP && check != 1){
              dir(0, -1);
              check = 0;
          } else if (g.keyCode == g.DOWN && check != 0){
              dir(0, 1);
              check = 1;
          } else if (g.keyCode == g.RIGHT && check != 2){
              dir(1, 0);
              check = 3;
          } else if (g.keyCode == g.LEFT && check != 3){
              dir(-1, 0);
              check = 2;
          }
      }
    void randomPos(){

        lowRef = 4f;
        highRef = (600-4*scl)/scl+0.1f;
        xHead = scl*(int)g.random(lowRef, highRef);
        yHead = scl*(int)g.random(lowRef, highRef);
        check = (int)(g.random(4));


        switch (check) {//der Kopf und der Schwanz der Schlange bekommen zufälige Koordinaten
            case 0:      // die Koordinaten sind von der Koordinaten des Kopfes abhängig
                xTail = xHead;
                yTail = yHead + scl;
                break;
            case 1:
                xTail = xHead;
                yTail = yHead - scl;
                break;
            case 2:
                xTail = xHead + scl;
                yTail = yHead;
                break;
            case 3:
                xTail = xHead - scl;
                yTail = yHead;
                break;
        }


    }

    void snakeMove(){
        xInt = xHead;
        yInt = yHead;
        xHead += xDir * scl;
        yHead += yDir * scl;

        for (int i = 0; i < snakeLength; i++) {
            if (i == 0){
                xIntA[i] = xCor[i];
                yIntA[i] = yCor[i];
                xCor[i] = xTail;
                yCor[i] = yTail;
            }else {
                xIntA[i] = xCor[i];
                yIntA[i] = yCor[i];
                xCor[i] = xIntA[i - 1];
                yCor[i] = yIntA[i - 1];
            }
        }
        if (g.keyCode == g.UP || g.keyCode == g.DOWN || g.keyCode == g.LEFT || g.keyCode == g.RIGHT){
            yTail = yInt;
            xTail = xInt;
        }
        xHead = g.constrain(xHead, 20f, 570f);
        yHead = g.constrain(yHead, 20f, 570f);

    }
    void erase(){
        for (int i = 0; i < snakeLength; i++) {
            xIntA[i] = 0f;
            yIntA[i] = 0f;
            xCor[i] = 0f;
            yCor[i] = 0f;
        }
        dir(0,0);
        snakeLength = 0;
    }
    void dir(float x, float y){
        xDir = x;
        yDir = y;
    }
    void bodyLength(int z){
          snakeLength = z;
      }
      boolean compare(){
          for (int i = 0; i < snakeLength; i++) {
              if(g.dist(xHead, yHead, xCor[i], yCor[i]) <= 1) return true;
          }
          return false;
      }

     void head(){
        g.rect(xHead, yHead, scl, scl);
        if (xTail != xHead) {
            g.ellipse(xHead + scl/2, yHead + scl/4, scl/2, scl/2);
            g.ellipse(xHead + scl/2, yHead + 3*scl/4, scl/2, scl/2);
        } else {
            g.ellipse(xHead + scl/4, yHead + scl/2, scl/2, scl/2);
            g.ellipse(xHead + 3*scl/4, yHead + scl/2, scl/2, scl/2);
        }

    }

    void tail(){
        g.rect(xTail, yTail, scl, scl);
    }



    void body(int i){

              if (xCor[i] != 0 && yCor[i] != 0)g.rect(xCor[i], yCor[i], scl, scl);

      }
    void draw(){
        head();
        tail();
        for (int i = 0; i < snakeLength; i++) {
        body(i);
        }
    }
}
