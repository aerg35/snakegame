package com.codesnakegame;

import com.sun.source.tree.Scope;
import processing.core.*;

import java.time.Duration;
import java.time.Instant;


public class SnakeSketch extends PApplet {
    public static void main(String[] args) {
        String[] appArgs = {"SnakeSketch"};
        SnakeSketch mySketch = new SnakeSketch();
        PApplet.runSketch(appArgs, mySketch);
    }
    private int white = color(255, 255, 255);
    private int red = color(255, 0, 0);
    private int brown = color(166, 42, 42);

    float scl = 10f;
    boolean testScore = true;
    boolean b = false;
    boolean ov = false;
    boolean startGame = false;
    boolean showTop = false;
    boolean nameClick = false;
    boolean openMenu = true;
    Instant now;
    Duration d;

    Instant time = Instant.now();
    Snake snake;
    Apple apple;
    Menu menu;
    GameOver over;
    Score score1;
    int score = 0;
    int speed = 150;
    String name = new String();
    PImage menuB, scoreB, startB, pic, coffeeBreak;

    private void wall(){
        /*quad(0,0, 10,0, 10,600, 0,600);
        quad(0,600, 0,590, 600,590, 600,600);
        quad(600,600, 590,600, 590,0, 600,0);
        quad(600,0, 600,10, 0,10, 0,0);*/
        fill(0);
        quad(20,20, 580,20, 580,580, 20,580);
        line(10,0, 10,600);
        line(590,0, 590,600);
        line(10,10, 590,10);
        line(10,590, 590,590);

        for (int i = 0; i <= 600; i += 20) {//die kleine horisontale Linien
            line(0,i, 10,i);
            line(590,i, 600,i);
            line(10,i+10, 20,i+10);
            line(580,i+10, 590,i+10);
        }
        for (int i = 30; i <= 590; i += 20) {
            line(i,0, i,10);
            line(i,590, i,600);
            line(i-10,10, i-10,20);
            line(i-10,580, i-10,590);
        }
    }


    public void eat(){
        float dis = dist(snake.xHead, snake.yHead, apple.xApple, apple.yApple);
        if (dis < 10){
            score++;
            snake.bodyLength(score);
            b = true;
            if (score%10 == 0){
                speed -= 5;
            }
        }
    }


    public void settings(){
        size(600,600);

    }

    public void gameOver(){
        ov = snake.compare();
        if (snake.xHead < 20.1f || snake.yHead < 20.1f || snake.xHead > 569.9f || snake.yHead > 569.9f ||  snake.compare()){
            ov = true;

            if(testScore){
                testScore = false;
                score1.snakeScore(name, score);
            }
            name = menu.clearName();
            over.gameOver(score);
            if (mouseX>236 && mouseX<365 && mouseY>400 && mouseY<454 && mousePressed && (mouseButton == LEFT)){//Menu button
                startGame = false;
                openMenu = true;
                score = 0;
            }
        }
    }

    public void setup(){
        background(brown);
        wall();
        menuB = loadImage("src/com/codesnakegame/Menu.jpg");
        startB = loadImage("src/com/codesnakegame/Start.jpg");
        scoreB = loadImage("src/com/codesnakegame/Score.jpg");
        pic = loadImage("src/com/codesnakegame/GameOver.jpg");
        coffeeBreak = loadImage("src/com/codesnakegame/CoffeeBreak.jpg");
        snake = new Snake(this, scl);
        apple = new Apple(this, scl);
        menu = new Menu(this, menuB, startB, scoreB);
        over = new GameOver(this, pic, menuB, coffeeBreak);
        score1 = new Score(this, menuB);
        menu.menu();
        menu.name();


    }

    @Override
    public void keyPressed() {

    }

    public void draw(){

        if (openMenu) {//shows the menu

            wall();
            menu.menu();
            menu.name();
            if (mouseX>195 && mouseX<404 && mouseY>350 && mouseY<397 && mousePressed && (mouseButton == LEFT)){//Start button
                testScore = true;
                speed = 150;
                snake.erase();
                startGame = true;
                openMenu = false;
                ov = false;
                snake.randomPos();
                apple.newApple();
            }else if (mouseX>206 && mouseX<394 && mouseY>450 && mouseY<516 && mousePressed && (mouseButton == LEFT)){//Score button
                showTop = true;
                openMenu = false;
            }else if (mouseX>200 && mouseX<400 && mouseY>250 && mouseY<300 && mousePressed && (mouseButton == LEFT)){//Fild for name
                nameClick = true;
                openMenu = false;
            }
        }else if (startGame){//starts the game
                gameOver();
             if (!ov) {
                now = Instant.now();
                d = Duration.between(time, now);
            if (d.toMillis() >= speed) {
                background(brown);
                wall();
                snake.keyPressed1();
                snake.snakeMove();
                fill(white);
                snake.draw();
                time = now;
            }
            if (b) {
                apple.newApple();
                b = false;
            }
            fill(red);
            apple.drawApple();
            eat();
        }
        }else if (showTop){//go to top 5 list
            wall();
            fill(red);
            score1.topList();
            if (mouseX>400 && mouseX<529 && mouseY>500 && mouseY<554 && mousePressed && (mouseButton == LEFT)){//Menu button
                showTop = false;
                openMenu = true;
                name = menu.clearName();
            }
        }else if(nameClick){//player can type shes/hes name
                name = menu.writeName();

                if (keyPressed && key == ENTER){
                    nameClick = false;
                    openMenu = true;
                }
            if (mouseX>195 && mouseX<404 && mouseY>350 && mouseY<397 && mousePressed && (mouseButton == LEFT)){//Start button
                testScore = true;
                speed = 150;
                snake.erase();
                startGame = true;
                openMenu = false;
                ov = false;
                snake.randomPos();
                apple.newApple();
            }else if (mouseX>206 && mouseX<394 && mouseY>450 && mouseY<516 && mousePressed && (mouseButton == LEFT)){//Score button
                showTop = true;
                openMenu = false;
            }
        }

    }
}