package com.codesnakegame;

import processing.core.PApplet;
import processing.core.PImage;

public class GameOver {
    PApplet g;
    PImage pic;
    PImage menuB;
    PImage coffeeBreak;
    GameOver(PApplet g, PImage pic, PImage menuB, PImage coffeeBreak){
        this.g = g;
        this.pic = pic;
        this.menuB = menuB;
        this.coffeeBreak = coffeeBreak;
    }
    float xG = 100, yG = 150;
    float xA = 100, yA = 230;
    float xM = 100, yM = 310;
    float xE1 = 100, yE1 = 390;
    float xO = 170, yO = 150;
    float xV = 170, yV = 230;
    float xE2 = 170, yE2 = 310;
    float xR = 170, yR = 390;

    void gameOver(int score){
        //Game Over picture

        g.image(pic, 150 ,70);
        //Coffee break picture

        g.image(coffeeBreak, 50, 300, 130, 216);
        //Menu button

        g.image(menuB, 236, 400, 129, 54);
        //Score
        g.textSize(20);
        g.text("SCORE: " + score, 250, 350);
    }


}