package com.codesnakegame;

import processing.core.PApplet;
import processing.core.PImage;

import java.util.Scanner;

public class Menu {
    PApplet g;
    PImage menuB;
    PImage startB;
    PImage scoreB;
    String name = "nickname";
    boolean isTypeName = true;
    int gray;

    Scanner scanner = new Scanner(System.in);
    boolean typeName = false;
    Menu(PApplet g, PImage menuB, PImage startB, PImage scoreB){
        this.g = g;
        this.gray = g.color(128, 128, 128);
        this.menuB = menuB;
        this.startB = startB;
        this.scoreB = scoreB;
    }
    int length;
    void menu(){
        //Menu button
        g.image(menuB, 170, 80, 259, 108);
        //Start button
        g.image(startB, 195, 350, 209, 47);
        //Score button
        g.image(scoreB, 206, 450, 188, 66);
    }
    String clearName(){
         name = "nickname";
        return name;
    }
    void name(){
        //Fild for name
        g.fill(gray);
        g.rect(200, 250, 200, 50);
        g.textSize(30);
        g.fill(0);
        g.text(name, 230,285);
    }
    int counter = 6;
    String writeName(){
        menu();
        name();
        if(isTypeName){
            name = "";
            isTypeName = false;
        }
        if (name.length() <= 6){
            if (g.keyPressed && counter == 6) {
                name += g.key;
                counter = 0;
            }else if (g.keyPressed) {
                counter += 1;
            }else if (g.keyPressed && g.key == g.BACKSPACE){
                length = name.length();
                name.substring(0, length-1);
            }
        }

        if (g.keyCode == g.ENTER) {
            isTypeName = true;

        }
        return name;
    }
}
